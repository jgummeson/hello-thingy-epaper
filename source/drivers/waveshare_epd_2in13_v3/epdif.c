/**
 *  @filename   :   epdif.cpp
 *  @brief      :   Implements EPD interface functions
 *                  Users have to implement all the functions in epdif.cpp
 *  @author     :   Yehui from Waveshare
 *
 *  Copyright (C) Waveshare     August 10 2017
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documnetation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to  whom the Software is
 * furished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS OR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */

#include <stdint.h>
#include "epdif.h"
#include "nrf_gpio.h"
#include "nrf_delay.h"
#include "nrf_drv_spi.h"

#define PAPER_SPI_INSTANCE 1

#define PAPER_SPI_MOSI_PIN 3
#define PAPER_SPI_SCK_PIN  4

static volatile bool paper_spi_xfer_done;  /**< Flag used to indicate that SPI instance completed the transfer. */

void paper_spi_event_handler(nrf_drv_spi_evt_t const * p_event, void * p_context){ paper_spi_xfer_done = true;}

static const nrf_drv_spi_t spi = NRF_DRV_SPI_INSTANCE(PAPER_SPI_INSTANCE);

int IfInit(void) {
    ret_code_t ret;

    nrf_drv_spi_config_t spi_config = NRF_DRV_SPI_DEFAULT_CONFIG;
    spi_config.ss_pin = CS_PIN;
    spi_config.mosi_pin = PAPER_SPI_MOSI_PIN;
    spi_config.sck_pin = PAPER_SPI_SCK_PIN;

    ret = nrf_drv_spi_init(&spi, &spi_config, paper_spi_event_handler, NULL);
    if (ret != NRF_SUCCESS) return ret;

    nrf_gpio_cfg_output(RST_PIN);
    nrf_gpio_cfg_output(DC_PIN);
    nrf_gpio_cfg_input(BUSY_PIN, NRF_GPIO_PIN_NOPULL);
    return 0;
}

void DigitalWrite(uint32_t pin, uint32_t value) {
    nrf_gpio_pin_write(pin, value);
}

uint32_t DigitalRead(uint32_t pin) {
    return nrf_gpio_pin_read(pin);
}

void DelayMs(uint32_t ms) {
    nrf_delay_ms(ms);
}

static unsigned char spi_tx_buff[2];
static unsigned char spi_rx_buff[2];

void SpiTransfer(unsigned char data) {
    ret_code_t ret;
    spi_tx_buff[0] = data;
    paper_spi_xfer_done = false;
    nrf_gpio_pin_clear(CS_PIN);
    ret = nrf_drv_spi_transfer(&spi, &spi_tx_buff[0], 1, &spi_rx_buff[0], 0);
    if (ret != NRF_SUCCESS) {
    	nrf_gpio_pin_set(CS_PIN);
    	return;
    }
    while(!paper_spi_xfer_done);
    nrf_gpio_pin_set(CS_PIN);
}

